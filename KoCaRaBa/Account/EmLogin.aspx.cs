﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using KoCaRaBa.Models;

namespace KoCaRaBa.Account
{
    public partial class EmLogin : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // RegisterHyperLink.NavigateUrl = "Register";
            // Enable this once you have account confirmation enabled for password reset functionality
            //ForgotPasswordHyperLink.NavigateUrl = "Forgot";
           // OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            }
        }

        protected void LogIn(object sender, EventArgs e)
        {
            clsCustomer cus = new clsCustomer(); 
            cus.CustomerEmail = Email.Text;
            cus.CustomerPass = Password.Text;

            if (cus.checkEmailExist())
            {
                int ci = cus.SecurityCheck();
                if (ci != 0)
                {
                    Session["SCuEmAd"] = cus.CustomerEmail;
                    Session["SCuCoAd"] = cus.CustomerId;
                    Session["SCuNaAd"] = cus.CustomerName;
                    Session["SEmNaAd"] = "employe";
                    
                    IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                    if (RememberMe.Checked == true)
                    {
                        Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}",
                                                        Request.QueryString["ReturnUrl"],
                                                        RememberMe.Checked),
                                          true);
                    }
                }
                else
                {
                    FailureText.Text = "Invalid login attempt";
                    ErrorMessage.Visible = true;
                }
            }
            else
            {
                FailureText.Text = "Invalid login attempt";
                ErrorMessage.Visible = true;
            }
                                       
        }

    }
}