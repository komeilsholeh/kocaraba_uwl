﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using KoCaRaBa.Models;

namespace KoCaRaBa.Account
{
    public partial class Register : Page
    {

        private string UserEmail, UserName;
        private int UserId;

        clsCustomer customer = new clsCustomer();

        protected void Page_Load(object sender, EventArgs e)
        {
            UserEmail = (string)(Session["SCuEmAd"]);
            UserName = (string)(Session["SCuNaAd"]);
            if ((Session["SCuCoAd"]) != null)
            {
                UserId = (int)(Session["SCuCoAd"]);
            }
            if (!IsPostBack)
            {
                if(!string.Equals(UserEmail,"") || !string.Equals(UserName, "") || UserId != 0)
                {
                    customer.CustomerEmail = UserEmail;
                    customer.CustomerName = UserName;
                    customer.CustomerId = UserId;
                    customer.getCustomerDetail();
                    Email.Text = customer.CustomerEmail;
                    txtName.Text = customer.CustomerName;
                    txtSurname.Text = customer.CustomerSurname;
                    txtTel.Text = customer.CustomerTel;
                    txtAddress.Text = customer.CustomerAddress;
                    txtPostcode.Text = customer.CustomerPostcode;
                } 
            }
        }
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            customer.CustomerId = UserId;
            customer.CustomerName = txtName.Text.Trim();
            customer.CustomerSurname = txtSurname.Text.Trim();
            customer.CustomerTel = txtTel.Text.Trim();
            customer.CustomerAddress = txtAddress.Text.Trim();
            customer.CustomerPostcode = txtPostcode.Text.Trim();
            customer.CustomerEmail = Email.Text;
            customer.CustomerPass = Password.Text;
            lblMessage.Text = customer.saveCustomer();
        }
    }
}