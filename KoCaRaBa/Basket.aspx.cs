﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KoCaRaBa
{
    public partial class Basket : System.Web.UI.Page
    {
        private string UserEmail, UserName;
        private int UserId;

        protected void Page_Load(object sender, EventArgs e)
        {
            UserEmail = (string)(Session["SCuEmAd"]);
            UserName = (string)(Session["SCuNaAd"]);
            if ((Session["SCuCoAd"]) != null)
            {
                UserId = (int)(Session["SCuCoAd"]);
            }
            Master.setLoginStatus();

        }      


        public string basketProducts()
        {
            string template = "error";
            string name, quantity, price;
            double total = 0.00;
            string image;


           


            


            StringWriter sW = new StringWriter();
            using (HtmlTextWriter writer = new HtmlTextWriter(sW))
            {
                int id;
               

                writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingTop, "40px");                
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "table table-striped table-hover");
                writer.AddAttribute(HtmlTextWriterAttribute.Width, "relative_length");
                writer.RenderBeginTag(HtmlTextWriterTag.Table);

                writer.RenderBeginTag(HtmlTextWriterTag.Tr);

                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write("   ");
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write("Name ");
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write("Quantity ");
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write("Price ");
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write("   ");
                writer.RenderEndTag();


                writer.RenderEndTag(); //tr ender L64

                clsBasket dbp = new clsBasket();
                DataSet ds = new DataSet();
                ds = dbp.getCustomerBasket(1);

                clsStock dbpr = new clsStock();
                DataSet ds2 = new DataSet();
                ds2 = dbpr.SelectAllInStock();

                int count;
                count = dbp.SelectCount();


                for (int i = 0; i < count; i++)
                {
                    id = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"]) - 1;
                    name = ds2.Tables[0].Rows[id]["ProductName"].ToString();
                    quantity = ds.Tables[0].Rows[i]["ProductQTY"].ToString();
                    price = Convert.ToDouble(ds2.Tables[0].Rows[id]["ProductPrice"]).ToString();
                    total += Convert.ToDouble(ds2.Tables[0].Rows[id]["ProductPrice"]) * Convert.ToDouble(ds.Tables[0].Rows[i]["ProductQTY"]);
                    image = "/images/" + ds2.Tables[0].Rows[id]["ProductImage"].ToString();


                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);

                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.AddAttribute(HtmlTextWriterAttribute.Src, image);
                    writer.AddAttribute(HtmlTextWriterAttribute.Width, "50");
                    writer.AddAttribute(HtmlTextWriterAttribute.Height, "50");
                    writer.AddAttribute(HtmlTextWriterAttribute.Alt, "");
                    writer.RenderBeginTag(HtmlTextWriterTag.Img);
                    writer.RenderEndTag();


                    writer.RenderEndTag();

                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(name);
                    writer.RenderEndTag();

                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(quantity);
                    writer.RenderEndTag();

                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write("\u00A3" +price);
                    writer.RenderEndTag();

                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.AddAttribute(HtmlTextWriterAttribute.Class, "btn btn-primary");
                    writer.AddAttribute(HtmlTextWriterAttribute.Id, "delete");                    
                    writer.RenderBeginTag(HtmlTextWriterTag.Button);
                    writer.Write("Delete");
                    writer.RenderEndTag();
                    writer.RenderEndTag();


                    writer.RenderEndTag(); //tr end tag L114

                    
                }

                
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);

                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("  ");
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(" ");
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("  ");
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("  ");
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("TOTAL: \u00A3" + total);
                writer.RenderEndTag();

                

                writer.RenderEndTag();
                writer.RenderEndTag();
                writer.RenderEndTag();

               
            }
            template = sW.ToString();
            return template;
        }

        protected void checkOut(object sender, EventArgs e)
        {
            Server.Transfer("Payment.aspx", true);
        }
    }
}