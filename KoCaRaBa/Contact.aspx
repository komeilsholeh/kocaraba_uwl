﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="KoCaRaBa.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>KoCaRaBa Shop </h3>
    <address>
        One Microsoft Way<br />
        University Of West London, W5 3RT,&nbsp; United Kingdom<br />
        <abbr title="Phone">Phone:</abbr>
        02088000000
    </address>

    <address>
        <strong>Support:</strong>   <a href="kocaraba@uwl.ac.uk">kocaraba@uwl.ac.uk</a><br />
        <strong>Marketing:</strong> <a href="kocaraba_marketing@uwl.ac.uk">kocaraba_marketing@uwl.ac.uk</a>
    </address>
</asp:Content>
