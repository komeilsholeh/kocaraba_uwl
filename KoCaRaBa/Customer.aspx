﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Customer.aspx.cs" Inherits="KoCaRaBa.Customer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <table style="width:95%;">
        <tr>            
            <td style="width:30%">Name</td>
            <td><asp:TextBox ID="txtName" runat="server" Width="95%"></asp:TextBox></td>
            <td style="width:30%"></td>
        </tr>
        <tr>            
            <td>Surname</td>
            <td> <asp:TextBox ID="txtSurname" runat="server" Width="95%"></asp:TextBox></td>
            <td style="width:30%"></td>
        </tr>
        <tr>          
            <td>Phone Number</td>
            <td> <asp:TextBox ID="txtTel" runat="server" Width="95%" TextMode="Phone"></asp:TextBox></td>
            <td style="width:30%"></td>
        </tr>
        <tr>          
            <td>Address</td>
            <td> <asp:TextBox ID="txtAddress" runat="server"  Width="95%"></asp:TextBox></td>
            <td style="width:30%"></td>
        </tr>
        <tr>          
            <td>Postcode</td>
            <td><asp:TextBox ID="txtPostcode" runat="server"  Width="95%"></asp:TextBox></td>
            <td style="width:30%"></td>
        </tr>
        <tr>          
            <td>Email Address</td>
            <td><asp:TextBox ID="txtEmail" runat="server" Width="95%" TextMode="Email"></asp:TextBox></td>
            <td style="width:30%"></td>
        </tr>
        <tr>          
            <td>Password</td>
            <td><asp:TextBox ID="txtPassword" runat="server" Width="95%" TextMode="Password"></asp:TextBox></td>
            <td style="width:30%">&nbsp;</td>
        </tr>
        <tr>          
            <td></td>
            <td>
                <asp:Button ID="btnSubmit" runat="server" Text="SUBMIT" OnClientClick="btnSubmit_Click" OnClick="btnSubmit_Click" />
                <asp:Label ID="lblSubmitMessage" runat="server"></asp:Label>
            </td>
            <td style="width:30%">&nbsp;</td>
             
        </tr>
    </table>
    
    </div>
    </form>
</body>
</html>
