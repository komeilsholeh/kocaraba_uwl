﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KoCaRaBa
{
    public partial class Customer : System.Web.UI.Page
    {
        private clsCustomer customer = new clsCustomer();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            customer.CustomerName = txtName.Text.Trim();
            customer.CustomerSurname = txtSurname.Text.Trim();
            customer.CustomerTel = txtTel.Text.Trim();
            customer.CustomerAddress = txtAddress.Text.Trim();
            customer.CustomerPostcode = txtPostcode.Text.Trim();
            customer.CustomerEmail = txtEmail.Text.Trim();
            customer.CustomerPass = txtPassword.Text.Trim();
            lblSubmitMessage.Text = customer.saveCustomer();
        }
    }
}