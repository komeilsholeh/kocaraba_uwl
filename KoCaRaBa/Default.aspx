﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="KoCaRaBa._Default" %>
<%@ MasterType VirtualPath="~/Site.Master" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server"> 

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <br />
                <p class="lead">Categories</p>

                <div id="listOfCategories" runat="server" class="list-group">
                      
                </div>
            </div>


            <div class="col-md-9">

                <br />

                <div class="row carousel-holder">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="images/main3.jpg" alt="" width="800" height="400">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="images/main6.jpg" alt="" width="800" height="400">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="images/main5.png" alt="" width="800" height="400">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>
                    
                </div>
                 
         </div>
         
            <div>
                <hr />
            
                &nbsp;&nbsp;<asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
 
            </div>
            <hr />
            <div id="divShowProduct" runat="server">
         
                            
            </div>
    </div>
   </div>

           
</asp:Content>
