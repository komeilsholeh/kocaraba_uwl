﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KoCaRaBa
{
    public partial class _Default : Page
    {
        private string UserEmail, UserName;
        private int UserId;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            
            
            if (!IsPostBack)
            {
                UserEmail = (string)(Session["SCuEmAd"]);
                UserName = (string)(Session["SCuNaAd"]);
                if ((Session["SCuCoAd"]) != null)
                {
                    UserId = (int)(Session["SCuCoAd"]);
                }
                Master.setLoginStatus();
                clsPListGenerator pl = new clsPListGenerator();
                string htm = pl.HtmlCodeCategoryMenu();
                listOfCategories.InnerHtml = htm;
            }


            string eventTarget = Request["__EVENTTARGET"];

            if (!string.Equals(eventTarget, null))
            {
                if (!string.Equals(eventTarget, ""))
                {
                    if (string.Equals(eventTarget.Substring(0, 6), "btnAdd"))
                    {
                        button_Add_Product_Click(eventTarget);            
                    } 
                    else if(string.Equals(eventTarget.Substring(0, 6), "CatLst"))
                    {
                        side_category_list_Click(eventTarget);
                        
                    }
                    else if (string.Equals(eventTarget , "btnSearch"))
                    {
                        search_product_button_Click();
                    }
                }

            }

        }


        protected void search_product_button_Click()
        {
            string proname = Request["txtSearch"];
            clsStock st = new clsStock();
            DataSet ds = new DataSet();
            clsPListGenerator pl = new clsPListGenerator();
            ds = st.SearchByWord(proname);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string htm = pl.HtmlCodeProducts(ds);
                System.Web.UI.HtmlControls.HtmlGenericControl prolist = new System.Web.UI.HtmlControls.HtmlGenericControl(htm);
                divShowProduct.InnerHtml = htm;
                lblMessage.Text = "  " + ds.Tables[0].Rows.Count.ToString() + " Item(s) Found.";
            }
            else
            {
                divShowProduct.InnerHtml = " ";
                lblMessage.Text = "Sorry Nothing With '" + proname + "' Found For Sale";
            }
        }

        protected void side_category_list_Click(string event_target)
        {
            int categoryId = Convert.ToInt32(event_target.Substring(6));
            clsStock st = new clsStock();
            DataSet ds = new DataSet();
            clsPListGenerator pl = new clsPListGenerator();
            ds = st.SelectByCategoryId(categoryId);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string htm = pl.HtmlCodeProducts(ds);
                System.Web.UI.HtmlControls.HtmlGenericControl prolist = new System.Web.UI.HtmlControls.HtmlGenericControl(htm);
                divShowProduct.InnerHtml = htm;
                lblMessage.Text = "  " + ds.Tables[0].Rows.Count.ToString() + " Item(s) Found For This Category.";
            }
            else
            {
                divShowProduct.InnerHtml = " ";
                lblMessage.Text = "Sorry Nothing Is Available For Sale In This Category.";
            }
        }

        protected void button_Add_Product_Click(string event_target)
        {
            UserEmail = (string)(Session["SCuEmAd"]);
            UserName = (string)(Session["SCuNaAd"]);
            if ((Session["SCuCoAd"]) != null)
            {
                UserId = (int)(Session["SCuCoAd"]);
            }
            string clickedButton = event_target;
            int productCode = Convert.ToInt32(clickedButton.Substring(6));
            string txtname = "txtAdd" + clickedButton.Substring(6);
            string txtvalue = Request[txtname];
            int productQty = Convert.ToInt16(txtvalue);
            clsBasket bs = new clsBasket();
            if (UserId>0)
            {
                bs.AddProductToBasket(UserId, productCode, productQty);
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('You Need To Login First');", true);
            }
        }

    }
}