﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Discount.aspx.cs" Inherits="KoCaRaBa.Discount" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <ul class="nav nav-pils nav-justified" style="padding-top: 40px">
        <li class="active"><a data-toggle="tab" href="#add">Add a new discount</a></li>
        <li><a data-toggle="tab" href="#delete">Delete a old discount</a></li>
    </ul>


    <div class="tab-content">

        <div id="add" class="tab-pane fade in active">
            <div class="section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">



                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="inputItemID" class="control-label">Item ID: </label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputItemID" placeholder="The id of the item" runat="server" name="inputItemID">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="inputDiscount" class="control-label">Discount: </label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputDiscount" placeholder="Between 5-75%" runat="server" name="inputDiscount">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="inputDays" class="control-label">Days: </label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="inputDays" placeholder="Number of days" runat="server" name="inputDays">
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default" runat="server" onserverclick="saveData">Apply Discount</button>





                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div id="delete" class="tab-pane fade">

            <%= deleteWriter() %>

            <div class="form-group">
                <div class="col-sm-2">
                    <label for="inputDelete" class="control-label">Discount ID: </label>
                </div>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputDelete" placeholder="Discount id to be deleted" runat="server" name="inputDelete">
                </div>
            </div>



            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-default" runat="server" onserverclick="deleteData">Delete Discount</button>

                </div>

            </div>
        </div>
    </div>

</asp:Content>
