﻿using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace KoCaRaBa
{
    public partial class Discount : System.Web.UI.Page
    {

        protected int id, amount, days;

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        public void saveData(object sender, EventArgs e)
        {

            int check;
            if (Int32.TryParse(inputDays.Value, out check))
            {
                days = check;
            }
            else
            {
                days = 1;
            }

            if (Int32.TryParse(inputItemID.Value, out check))
            {
                id = check;
            }
            else
            {
                id = 1;
            }

            if (Int32.TryParse(inputDiscount.Value, out check))
            {
                amount = check;
            }
            else
            {
                amount = 0;
            }


            DateTime myDateTime = DateTime.Now;

            clsDBDiscount dbp = new clsDBDiscount();
            clsDiscount discount = new clsDiscount();

            discount.ProductId = id;
            discount.DiscountStartDate = myDateTime.ToString("yyyy-MM-dd HH:mm:ss");
            discount.DiscountEndDate = myDateTime.AddDays(days).ToString("yyyy-MM-dd HH:mm:ss");
            discount.DiscountPercentage = amount;
            discount.DiscountValid = 1;

            dbp.addDiscount(discount);


        }



        public void deleteData(object sender, EventArgs e)
        {
            int check, delete = 0;
            if (Int32.TryParse(inputDelete.Value, out check))
            {
                delete = check;
            }
            else
            {
                Response.Write("<script LANGUAGE='JavaScript'>alert('Please enter a number')</script>");
            }

            clsDB db = new clsDB();
            db.DBExecute("DELETE FROM tblDiscount WHERE DiscountId= " + delete);


        }



        public string deleteWriter()
        {

            string template = "error";
            int discountId, productId, perc;
            DateTime endDate;


            clsDiscount dbp = new clsDiscount();
            DataSet ds = new DataSet();
            ds = dbp.getData();



            int count;
            count = dbp.SelectCount();


            StringWriter sW = new StringWriter();
            using (HtmlTextWriter writer = new HtmlTextWriter(sW))
            {

                writer.AddStyleAttribute(HtmlTextWriterStyle.PaddingBottom, "25px");
                writer.RenderBeginTag(HtmlTextWriterTag.Div);

                writer.AddAttribute(HtmlTextWriterAttribute.Class, "table table-striped table-hover");
                writer.AddAttribute(HtmlTextWriterAttribute.Width, "relative_length");
                writer.RenderBeginTag(HtmlTextWriterTag.Table);

                writer.RenderBeginTag(HtmlTextWriterTag.Tr);



                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write("DiscountId ");
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write("ProductId ");
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write("Amount ");
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write("End Date   ");
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write("   ");
                writer.RenderEndTag();


                writer.RenderEndTag(); //tr ender L123


                for (int i = 0; i < count; i++)
                {
                    discountId = Convert.ToInt32(ds.Tables[0].Rows[i]["DiscountId"]);
                    productId = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"]);
                    perc = Convert.ToInt32(ds.Tables[0].Rows[i]["DiscountPercent"]);
                    endDate = Convert.ToDateTime(ds.Tables[0].Rows[i]["DiscountFinishDate"]);


                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);

                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(discountId);
                    writer.RenderEndTag();

                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(productId);
                    writer.RenderEndTag();

                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(perc);
                    writer.RenderEndTag();

                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(endDate);
                    writer.RenderEndTag();

                    writer.RenderEndTag(); //tr end tag L159



                }



            }
            template = sW.ToString();
            return template;
        }



    }
}