﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KoCaRaBa
{
    public partial class Foods : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            clsStock dbp = new clsStock();
            DataSet ds = new DataSet();
            ds = dbp.SelectAllInStock();

            name.Text = ds.Tables[0].Rows[0]["ProductName"].ToString();
            description.Text = ds.Tables[0].Rows[0]["ProductDescription"].ToString();
            double temp = Convert.ToDouble(ds.Tables[0].Rows[0]["ProductPrice"]);
            temp = Math.Round(temp, 2);
            price.Text = temp.ToString();
            image1.ImageUrl= ds.Tables[0].Rows[0]["ProductImage"].ToString();




            name2.Text = ds.Tables[0].Rows[1]["ProductName"].ToString();
            description2.Text = ds.Tables[0].Rows[1]["ProductDescription"].ToString();
            temp = Convert.ToDouble(ds.Tables[0].Rows[1]["ProductPrice"]);
            temp = Math.Round(temp, 2);
            price2.Text = temp.ToString();
            image2.ImageUrl = ds.Tables[0].Rows[1]["ProductImage"].ToString();
            
        }

    

        protected void Button1C(object sender, EventArgs e)
        {
            clsDBBasket dbp = new clsDBBasket();
            clsBasket basket = new clsBasket();

            Int32 prodId, prodQTY, custmId;
            prodId = 1;
            prodQTY = Int32.Parse(drop.SelectedValue);
            custmId = 1;

            basket.CustomerId = custmId;
            basket.ProductId = prodId;
            basket.ProductQTY = prodQTY;


            dbp.addBasket(basket);

        }


        protected void Button2C(object sender, EventArgs e)
        {
            clsDBBasket dbp = new clsDBBasket();
            clsBasket basket = new clsBasket();

            Int32 prodId, prodQTY, custmId;
            prodId = 2;
            prodQTY = Int32.Parse(drop2.SelectedValue);
            custmId = 1;

            basket.CustomerId = custmId;
            basket.ProductId = prodId;
            basket.ProductQTY = prodQTY;


            dbp.addBasket(basket);

        }



    }
}