﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="KoCaRaBa.Payment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    
     <br />
       <div style="padding:25px">
    <asp:Label ID="Confirmation" runat="server" Visible="false" Text="Your order has been received and paid." CssClass=" btn-lg" Font-Size="Large" Font-Bold="True"></asp:Label>
       </div>
        <br />
     <div style="padding-left:200px">
        <asp:Button ID="continueShopping" runat="server" Text="Continue Shopping"   Visible="false" OnClick="continueShopping_Click" CssClass="btn-warning btn-lg" Font-Size="Large" ForeColor="Black" />
         </div>
   

    <div class="form-group" id="OrderDetails" runat="server"> 
    
<h2>Payment details</h2>
<h3>How would you like to pay?</h3>

<div class="form-group">
    <br />
      <label for="cardType" class="col-lg-1 ">Card type</label>
      <br />
      <div class="col-xs-3">
        <select class="form-control" id="cardType">
          <option>Visa/Delta/Electron</option>
          <option>Master Card</option>
          <option>Maestro</option>
        </select>
      </div>
</div>
<br />
<div class="form-group">
  <label class="control-label" for="cardNumber">Card number</label>
  <br />
  <input class="form-control" id="cardNumber" type="text" >
</div>
<div class="form-group">
  <label class="control-label" for="cardName">Name on card</label>
  <br />
  <input class="form-control" id="cardName" type="text" >
  <p><small>Full name as printed on your card</small></p>
</div>

    <label for="month" class="col-lg-1">Exp date</label>
    
        
       
      <div class="col-xs-2">
        <select class="form-control" id="month"  >
          <option>Month</option>
          <option>01</option>
          <option>02</option>
          <option>03</option>
          <option>04</option>
          <option>05</option>
          <option>06</option>
          <option>07</option>
          <option>08</option>
          <option>09</option>
          <option>10</option>
          <option>11</option>
          <option>12</option>
        </select>
        </div>
     

      

      <div class="col-xs-2">
          <select class="form-control" id="year">
          <option>Year</option>
          <option>16</option>
          <option>17</option>
          <option>18</option>
          <option>19</option>
          <option>20</option>
          <option>21</option>
          <option>22</option>
          <option>23</option>
          <option>24</option>
          <option>25</option>
          <option>26</option>
          <option>27</option>
        </select>
     </div>


    <br />
<div class="form-group">
    <br />
  <label class="control-label" for="securityCode">Security Code</label>
  <br />
  <input class="form-control" id=Text1 type="text" >
    <img src="images/securitycode.gif" alt="" style="width:150px; height:75px; " />
  <p><small>Last 3 digits located in the signature panel on the back of your card</small></p>
</div>



<h3>Delivery Details </h3>
<div class="form-group">
  <label class="control-label" for="Address">Address line 1</label>
  <br />
  <input class="form-control" id=Address1 type="text" >
  
</div>
<div class="form-group">
  <label class="control-label" for="Address">Address line 2 (optional)</label>
  <br />
  <input class="form-control" id=Address2 type="text" >
</div>
<div class="form-group">
  <label class="control-label" for="town">Town</label>
  <br />
  <input class="form-control" id=Town type="text" >
</div>
<div class="form-group">
  <label class="control-label" for="Postcode">Postcode</label>
  <br />
  <input class="form-control" id=Postcode type="text" >
</div>


   <div style="padding-left:200px">

        <asp:Button ID="order" runat="server" Height="46px" Text="Order&Pay" Width="138px" OnClick="order_Click" Font-Bold="True" CssClass="btn-lg" ForeColor="Black" />
    </div>
       </div> 

   </div> 


</asp:Content>
