﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace KoCaRaBa
{
    public partial class Payment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            order.Click += new EventHandler(this.order_Click);
            continueShopping.Click += new EventHandler(this.continueShopping_Click);

        }

        protected void order_Click(object sender, EventArgs e)
        {
            Button clickedButton = (Button)sender;
            clickedButton.Enabled = false;
            Confirmation.Visible = true;
            OrderDetails.Visible = false;
            continueShopping.Visible = true;

            clsBasket dbp = new clsBasket();
            DataSet ds = new DataSet();
            ds = dbp.getCustomerBasket(1);

            clsStock dbp2 = new clsStock();
            DataSet ds2 = new DataSet();
            ds2 = dbp2.SelectAllInStock();

            clsDBPayment dbp3 = new clsDBPayment();
            clsPayment payment = new clsPayment();


            int count;
            count = dbp.SelectCount();

            for (int i = 1; i < count; i++)
            {
                int id = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductId"].ToString());
                payment.ProductId = id;
                payment.ProductQTY = Convert.ToInt32(ds.Tables[0].Rows[i]["ProductQTY"].ToString());
                payment.SoldPrice = Convert.ToDouble(ds2.Tables[0].Rows[id]["ProductPrice"].ToString());

                dbp3.addPayment(payment);
            }

            clsDB db = new clsDB();
            db.DBExecute("DELETE FROM tblBasketProduct");

        }

        protected void continueShopping_Click(object sender, EventArgs e)
        {
            Server.Transfer("Default.aspx", true);
       
        }
    }
}