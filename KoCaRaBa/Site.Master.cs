﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;

namespace KoCaRaBa
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
    //    private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;

        private string UserEmail, UserName,EmployUser;
        private int UserId;

        protected void Page_Init(object sender, EventArgs e)
        {
            // The code below helps to protect against XSRF attacks
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                // Use the Anti-XSRF token from the cookie
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                // Generate a new Anti-XSRF token and save to the cookie
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
             //   ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue)
                 //   || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            UserEmail = (string)(Session["SCuEmAd"]);
            UserName = (string)(Session["SCuNaAd"]);
            EmployUser= (string)(Session["SEmNaAd"]);

            if ((Session["SCuCoAd"]) != null)
            {
               UserId = (int)(Session["SCuCoAd"]);
            }
            
            setLoginStatus();
           
            
        }

        protected void basketImg_Click(object sender, EventArgs e)
        {
            UserEmail = (string)(Session["SCuEmAd"]);
            UserName = (string)(Session["SCuNaAd"]);
            if ((Session["SCuCoAd"]) != null)
            {
                UserId = (int)(Session["SCuCoAd"]);
            }
            if (UserId>0)
            {
                Response.Redirect("~/Basket");
            }
        }

        public void setLoginStatus()
        {
            if (!getLoginStatus())
            {
                lblLogin.Text = "Log In";
                lnkLogin.HRef = "~/Account/Login";
            }
            else 
            {
                lblLogin.Text = "Hello! " + UserName;
                lnkLogin.HRef = "~/Account/Register";
            }
            if (string.Equals(EmployUser, "employe"))
            {
                lblLogin.Text = "Hello! " + UserName + "(Employee)";
                drpConfigure.Visible = true;
            }            
        }
        public bool getLoginStatus()
        {
            bool res = false;
            if (!string.Equals(UserEmail, "") || UserEmail.Length > 0)
            {
                if (!string.Equals(UserName, "") || UserName.Length > 0)
                {
                    if (UserId>0)
                    {
                        res = true;
                    }
                }
            }
       
                return res;
        }

        public string WhoLogined
        {
            get { return lblLogin.Text; }
            set { lblLogin.Text = value; }
        }

        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }
    }

}