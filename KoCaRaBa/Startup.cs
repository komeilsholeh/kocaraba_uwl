﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KoCaRaBa.Startup))]
namespace KoCaRaBa
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
