﻿<%@ Page Title="Product In Stock" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StockList.aspx.cs" Inherits="KoCaRaBa.StockList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
     <div class="container">
        <div class="row">

            <div>
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="ProductId" DataSourceID="SDSStockList" Width="99%" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="ProductId" HeaderText="ProductId" ReadOnly="True" SortExpression="ProductId" />
                        <asp:BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName" />
                        <asp:BoundField DataField="CategoryName" HeaderText="CategoryName" SortExpression="CategoryName" />
                        <asp:BoundField DataField="ProductSold" HeaderText="Sold" SortExpression="ProductSold" />
                        <asp:BoundField DataField="ProductStock" HeaderText="Available" ReadOnly="True" SortExpression="ProductStock" />
                        <asp:BoundField DataField="ProductMin" HeaderText="Minimum" SortExpression="ProductMin" />
                        <asp:BoundField DataField="ProductUnit" HeaderText="Unit" SortExpression="ProductUnit" />
                        <asp:BoundField DataField="ProductPrice" HeaderText="Price" SortExpression="ProductPrice" />
                        
                    </Columns>
                    <FooterStyle BackColor="#CCCC99" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <RowStyle BackColor="#F7F7DE" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FBFBF2" />
                    <SortedAscendingHeaderStyle BackColor="#848384" />
                    <SortedDescendingCellStyle BackColor="#EAEAD3" />
                    <SortedDescendingHeaderStyle BackColor="#575357" />
                </asp:GridView>


                <asp:SqlDataSource ID="SDSStockList" runat="server" ConnectionString="<%$ ConnectionStrings:DBKOCARABAConnectionString1 %>" SelectCommand="SELECT [ProductId], [ProductStock], [ProductSold], [ProductName], [ProductUnit], [ProductMin], [CategoryName], [ProductPrice] FROM [qryProductStock]"></asp:SqlDataSource>


            </div>


        </div>
    </div>

</asp:Content>
