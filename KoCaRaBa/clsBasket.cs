﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace KoCaRaBa
{
    public class clsBasket
    {

        private Int32 productId, productQTY, customerId;


        public void AddProductToBasket(int customerId, int productId, int productQTY)
        {
            clsDB db = new clsDB();
            db.DBExecute("INSERT INTO tblBasketProduct(CustomerId,ProductId,ProductQTY) VALUES(" + customerId + ", " + productId + ", " + productQTY + ")");
        }

        public void UpdateProductBasket(int customerId, int productId, int productQTY)
        {
            clsDB db = new clsDB();
            db.DBExecute("UPDATE tblBasketProduct SET ProductQTY= " + productQTY + " WHERE CustomerId= " + customerId + " AND ProductId= " + productId);
        }

        public void DeleteProductFromBasket(int customerId, int productId)
        {
            clsDB db = new clsDB();
            db.DBExecute("DELETE FROM tblBasketProduct WHERE CustomerId= " + customerId + " AND ProductId= " + productId);
        }

        public DataSet getCustomerBasket(int customerId)
        {
            DataSet ds;
            clsDB db = new clsDB();
            ds = db.SelectCommand("SELECT * FROM qryCustomerBasket WHERE CustomerId= " + customerId);
            return ds;
        }

        public void DeleteCustomerBasket(int customerId)
        {
            clsDB db = new clsDB();
            db.DBExecute("DELETE FROM tblBasketProduct WHERE CustomerId= " + customerId);
        }

        public int SelectCount()
        {           
            int count = 0;
            DataSet ds;
            clsDB db = new clsDB();
            ds = db.SelectCommand("SELECT COUNT(*) FROM tblBasketProduct");
            count = int.Parse(ds.Tables[0].Rows[0][0].ToString());
            return count;
        }

        public int ProductId
            {
                get
                {
                    return productId;
                }

                set
                {
                    productId = value;
                }
            }

            public int ProductQTY
            {
                get
                {
                    return productQTY;
                }

                set
                {
                    productQTY = value;
                }
            }

           
            public int CustomerId
            {
                get
                {
                    return customerId;
                }

                set
                {
                    customerId = value;
                }
            }
        

    }
}