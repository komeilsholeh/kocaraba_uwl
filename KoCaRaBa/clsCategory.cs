﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace KoCaRaBa
{
    public class clsCategory
    {
        private int categoryId;
        private string categoryName, categoryDescription;

        public bool AddCategory()
        {
            clsDB db = new clsDB();
            DataSet ds = new DataSet();
            bool respond = false;
            if (!string.Equals(CategoryName.Trim(), ""))
            {
                if (CategoryId == 0)
                {
                    if (!IsCategoryExist())
                    {
                        CategoryId = getNewCategoryId();
                        db.DBExecute("INSERT INTO tblCategory(CategoryId,CategoryName,CategoryDescription) VALUES(" + categoryId + ",'" + categoryName + "','" + categoryDescription + "')");
                        respond = true;
                    }
                }             
            }
            return respond;
        }

        public void UpdateCategory()
        {
            clsDB db = new clsDB();
            DataSet ds = new DataSet();
            if (string.Equals(CategoryName.Trim(), ""))
            {
                if (CategoryId > 0)
                {
                    if (!IsCategoryExist())
                    {
                        db.DBExecute("UPDATE tblCategory SET CategoryName='" + CategoryName + "',CategoryDescription='" + categoryDescription + "' WHERE CategoryId=" + categoryId);
                    }
                }
            }
        }

        public void DeleteCategory(int customerId, int productId)
        {
            if (!IsCategoryUsed())
            { 
                clsDB db = new clsDB();
                db.DBExecute("DELETE FROM tblCategory WHERE CategoryId= " + CategoryId);
            }
        }


        public DataSet getCategoriesList()
        {
            DataSet ds = new DataSet();
            clsDB db = new clsDB();
            ds = db.SelectCommand("SELECT CategoryId,CategoryName FROM tblCategory");
            return ds;
        }

        private bool IsCategoryUsed()
        {
            bool used = false;
            DataSet ds = new DataSet();
            clsDB db = new clsDB();
            if (CategoryId == 0) ds = db.SelectCommand("SELECT CategoryId FROM tblProduct WHERE CategoryId=" + CategoryId);
            if (ds.Tables[0].Rows.Count > 0) used = true;
            return used;
        }

        private bool IsCategoryExist()
        {
            bool exist = false;
            DataSet ds = new DataSet();
            clsDB db = new clsDB();
            if (CategoryId == 0) ds = db.SelectCommand("SELECT CategoryId FROM tblCategory WHERE CategoryName='"+ CategoryName +"'");
            if (CategoryId > 0) ds = db.SelectCommand("SELECT CategoryId FROM tblCategory WHERE CategoryName='" + CategoryName + "' AND CategoryId<>" + CategoryId);
            if (ds.Tables[0].Rows.Count > 0) exist = true;
            return exist;
        } 

        private int getNewCategoryId()
        {
            clsDB db = new clsDB();
            DataSet ds = db.SelectCommand("SELECT MAX(CategoryId) AS Categoryid FROM tblCategory");
            int categoryId = Convert.ToInt16(ds.Tables[0].Rows[0]["Categoryid"]) + 1;
            return categoryId;
        }
        public string CategoryDescription
        {
            get
            {
                return categoryDescription;
            }

            set
            {
                categoryDescription = value;
            }
        }

        public int CategoryId
        {
            get
            {
                return categoryId;
            }

            set
            {
                categoryId = value;
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }

            set
            {
                categoryName = value;
            }
        }
    }
}