﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KoCaRaBa
{
    class clsCheckout
    {
        public void customerCheckout(int customerId, string paymentMethod, string deliveryMethod)
        {
            clsBasket basket = new clsBasket();
            DataSet ds = new DataSet();
            clsDB db = new clsDB();
            string dtt;
            dtt = DateTime.Now.ToLongDateString();
            int sellid = getNewSellingId();
            db.DBExecute("INSERT INTO tblSelling(SellingId,CustomerId,SellingDate,PaymentMethod,DeliveryStatus) VALUES("+ sellid +","+ customerId +"," + dtt + ",'" + paymentMethod + "','"+ deliveryMethod +"')");

            ds = basket.getCustomerBasket(customerId);
            Int32 proId;
            int proQTY;
            Double proPrice;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                proId = Convert.ToInt16(ds.Tables[0].Rows[i]["ProductId"]);
                proQTY= Convert.ToInt16(ds.Tables[0].Rows[i]["ProductQTY"]);
                proPrice= Convert.ToDouble(ds.Tables[0].Rows[i]["ProductPrice"]);
                db.DBExecute("INSERT INTO tblSellingProduct(SellingId,ProductId,ProductQTY,SoldPrice) VALUES("+ sellid + "," + proId + "," + proQTY + "," + proPrice + ")");
            }
            clsBasket bs = new clsBasket();
            bs.DeleteCustomerBasket(customerId);
        }
        private int getNewSellingId()
        {
            clsDB db = new clsDB();
            DataSet ds = db.SelectCommand("SELECT MAX(SellingId) AS Sellingid FROM tblSelling");
            int sellingId = Convert.ToInt16(ds.Tables[0].Rows[0]["Sellingid"]) + 1;
            return sellingId;
        }

    }
}
