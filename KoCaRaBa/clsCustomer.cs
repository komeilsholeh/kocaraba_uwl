﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace KoCaRaBa
{
    public class clsCustomer
    {
        private int customerId;
        private string customerName, customerSurname, customerTel, customerAddress, customerPostcode, customerEmail, customerPass;

        public string saveCustomer()
        {
            string sysmsg = checkCustomerDetail();
            if (string.Equals(sysmsg, ""))
            {
                clsDB db = new clsDB();
                if (customerId==0)
                {
                    int cusid = getNewCustomerId();
                    db.DBExecute("INSERT INTO tblCustomer(CustomerId,CustomerName,CustomerSurname,CustomerTel,CustomerAddress,CustomerPostCode,CustomerEmail,CustomerPass) VALUES(" + cusid + ", '" + customerName + "', '" + customerSurname + "', '" + customerTel + "', '" + customerAddress + "', '" + customerPostcode + "', '" + customerEmail + "', '" + customerPass + "')");
                    customerId = cusid;
                    sysmsg = "You Are Now Registered";
                }
                else
                {
                    DataSet ds = db.SelectCommand("SELECT CustomerId FROM tblCustomer WHERE CustomerEmail='" + CustomerEmail + "' AND CustomerId<>" + customerId);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        sysmsg = "This Email Address Is Using By Another Customer";
                    }
                    else
                    {
                        db.DBExecute("UPDATE tblCustomer SET CustomerName='" + CustomerName + "',CustomerSurname='" + CustomerSurname + "',CustomerTel='" + CustomerTel + "',CustomerAddress='" + CustomerAddress + "',CustomerPostCode='" + CustomerPostcode + "',CustomerEmail='" + CustomerEmail + "',CustomerPass='" + customerPass + "' WHERE CustomerId= " + CustomerId);
                        sysmsg = "Your detail is now updated";                        
                    }
                }
            }
            return sysmsg;
        }

        public void DeleteCustomer(int customerId, int productId)
        {
            clsDB db = new clsDB();
            db.DBExecute("DELETE FROM tblCustomer WHERE CustomerId= " + customerId);
        }

        public Boolean checkEmailExist()
        {
            Boolean emailexist = false;
            clsDB db = new clsDB();
            DataSet ds = db.SelectCommand("SELECT CustomerId FROM tblCustomer WHERE CustomerEmail='"+ CustomerEmail +"'");
            if (ds.Tables[0].Rows.Count > 0)
            {

                emailexist = true;
            }
            return emailexist;
        }

        private int getNewCustomerId()
        {
            clsDB db = new clsDB();
            DataSet ds = db.SelectCommand("SELECT MAX(CustomerId) AS Customerid FROM tblCustomer");
            int customerId = Convert.ToInt16(ds.Tables[0].Rows[0]["Customerid"]) + 1;
            return customerId;
        }

        private string checkCustomerDetail()
        {
            string sysmsg = "";
            if (String.Equals(CustomerName, ""))
            {
                sysmsg = "please enter your name. ";
            }
            else
            {
                if (String.Equals(CustomerEmail, ""))
                {
                    sysmsg = "please enter your email address. ";
                }
                else
                {
                    if (CustomerId==0 && checkEmailExist() == true)
                    {
                        sysmsg = "Email Address Already Exist. ";
                    }
                    else
                    {
                        if (string.Equals(customerPass, "") && customerPass.Length < 6)
                            sysmsg = "please enter your password. it should be at least 6 character long. ";
                    }
                }
            }
            return sysmsg;
        }

        public int SecurityCheck()
        {
            int cod = 0;
            CustomerId = SecurityDBCheck();
            if (CustomerId != 0)
            {
                getCustomerDetail();
                cod = CustomerId;
            }
            return cod;
        }

        public void getCustomerDetail()
        {
            clsDB db = new clsDB();
            DataSet ds = db.SelectCommand("SELECT * FROM tblCustomer WHERE CustomerEmail='" + customerEmail + "'");
            if (ds.Tables[0].Rows.Count == 1)
            {
                CustomerId = Convert.ToInt16(ds.Tables[0].Rows[0]["CustomerId"]);
                CustomerName = ds.Tables[0].Rows[0]["CustomerName"].ToString();
                CustomerSurname = ds.Tables[0].Rows[0]["CustomerSurname"].ToString();
                CustomerTel = ds.Tables[0].Rows[0]["CustomerTel"].ToString();
                CustomerAddress = ds.Tables[0].Rows[0]["CustomerAddress"].ToString();
                CustomerPostcode = ds.Tables[0].Rows[0]["CustomerPostCode"].ToString();
                CustomerEmail = ds.Tables[0].Rows[0]["CustomerEmail"].ToString();
            }
        }

        private int SecurityDBCheck()
        {
            clsDB db = new clsDB();
            DataSet ds = db.SelectCommand("SELECT CustomerId FROM tblCustomer WHERE CustomerEmail='" + CustomerEmail + "' AND CustomerPass='"+ customerPass +"'");
            int cui = 0;
            if (ds.Tables[0].Rows.Count == 1)
            {
                cui = Convert.ToInt16(ds.Tables[0].Rows[0]["CustomerId"]);
            }
            return cui;
        }

        public string CustomerName
        {
            get
            {
                return customerName;
            }

            set
            {
                customerName = value;
            }
        }

        public string CustomerSurname
        {
            get
            {
                return customerSurname;
            }

            set
            {
                customerSurname = value;
            }
        }

        public string CustomerTel
        {
            get
            {
                return customerTel;
            }

            set
            {
                customerTel = value;
            }
        }

        public string CustomerAddress
        {
            get
            {
                return customerAddress;
            }

            set
            {
                customerAddress = value;
            }
        }

        public string CustomerPostcode
        {
            get
            {
                return customerPostcode;
            }

            set
            {
                customerPostcode = value;
            }
        }

        public string CustomerEmail
        {
            get
            {
                return customerEmail;
            }

            set
            {
                customerEmail = value;
            }
        }

        public string CustomerPass
        {
            set
            {
                customerPass = value;
            }
        }

        public int CustomerId
        {
            get
            {
                return customerId;
            }
            set
            {
                customerId = value;
            }
        }


    }
}