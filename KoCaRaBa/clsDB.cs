﻿using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace KoCaRaBa
{

    // clsDB
    // this class can connect to diffrent databases, we only need to change the data source in connection string
    class clsDB
    {
        private string strConnection()
        {
            string strconnection = ConfigurationManager.ConnectionStrings["DBKOCARABAConnectionString1"].ToString();
            return strconnection;
        }

        // DBConnection makes a connection to database 
        // It return a connection whenever we call it

        public SqlConnection DBConnection()
        {
            string strconnection = strConnection();
            SqlConnection DBConnection = new SqlConnection(strconnection);
            return DBConnection;
        }

        // DBExecute is for doing actions such as INSERT, UPDATE, DELETE and CREATE on tables in Database
        // it gets the command in CommandString and it doesn not return any data.

        public void DBExecute(String CommandString)
        {

            SqlCommand command = new SqlCommand();
            command.CommandType = CommandType.Text;
            command.CommandText = CommandString;
            command.Connection = DBConnection();
            command.Connection.Open();
            command.ExecuteNonQuery();
            command.Connection.Close();

        }

        // SelectCommand is for SELECT command
        // it gets SELECT command from SelectString and return a DataSet with results

        public DataSet SelectCommand(String SelectString)
        {            
            DataSet ds = new DataSet();
            SqlDataAdapter dataadapter = new SqlDataAdapter(SelectString, DBConnection());
            dataadapter.Fill(ds);
            dataadapter.Dispose();
            return ds;
        }

        public void insertCommand(SqlCommand insCommand)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand command = new SqlCommand();
            command = insCommand;
            SqlConnection connection = new SqlConnection();
            connection = DBConnection();
            connection.Open();
            command.Connection = connection;
            adapter.InsertCommand = command;
            adapter.InsertCommand.ExecuteNonQuery();
            connection.Close();
        }

        public void updateCommand(SqlCommand updateCommand)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand command = updateCommand;
            command.Connection.ConnectionString = strConnection();
            adapter.UpdateCommand = command;
            adapter.UpdateCommand.ExecuteNonQuery();
        }

        public void deleteCommand(SqlCommand deleteCommand)
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand command = deleteCommand;
            command.Connection.ConnectionString = strConnection();
            adapter.UpdateCommand = command;
            adapter.DeleteCommand.ExecuteNonQuery();
        }

    }
}
