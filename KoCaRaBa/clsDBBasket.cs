﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace KoCaRaBa
{
    public class clsDBBasket
    {
        public void addBasket(clsBasket basket)
        {
            
            SqlCommand command = new SqlCommand("INSERT INTO tblBasketProduct(CustomerId, ProductId, ProductQTY) VALUES(@customerId,@productId,@productQTY)");

            // Add the parameters for the InsertCommand.
            
            command.Parameters.AddWithValue("@customerId", basket.CustomerId);
            command.Parameters.AddWithValue("@productId",  basket.ProductId);            
            command.Parameters.AddWithValue("@productQTY", basket.ProductQTY);
           

            clsDB db = new clsDB();
            db.insertCommand(command);
        }

        public void updateBasket(clsBasket basket)
        {
            clsBasket newbasket = basket;
            SqlCommand command = new SqlCommand("UPDATE tblBasketProduct(CustomerId, ProductId, ProductQTY) VALUES(@customerId, @productId, @productQTY)");

            // Add the parameters for the UpdateCommand.
            //, ,,,,,

            
            command.Parameters.Add("@productId", SqlDbType.Int, newbasket.ProductId);
            command.Parameters.Add("@productQTY", SqlDbType.Int, newbasket.ProductQTY);

            SqlParameter parameter = command.Parameters.Add("@productId", SqlDbType.Int, newbasket.CustomerId);

            clsDB db = new clsDB();
            db.updateCommand(command);
        }

        public void deleteBasket(clsBasket basket)
        {
            clsBasket newbasket = basket;
            SqlCommand command = new SqlCommand("DELETE FROM tblBasket WHERE CustomerId=@basketId");
            
            SqlParameter parameter = command.Parameters.Add("@customerId", SqlDbType.Int, newbasket.CustomerId);

            clsDB db = new clsDB();
            db.updateCommand(command);
        }

       public void deleteProductFromBasket(clsBasket basket)
        {
            clsBasket newbasket = basket;
            SqlCommand command = new SqlCommand("DELETE FROM tblBasket WHERE ProductID=@productId AND CustomerId=@customerId");

            SqlParameter parameter = command.Parameters.Add("@productId", SqlDbType.Int, newbasket.ProductId);

        }

        public DataSet SelectAllByCustomer(int customerId)
        {
            DataSet ds;
            clsDB db = new clsDB();
            ds = db.SelectCommand("SELECT * FROM tblBasketProduct WHERE CustomerId = "+ customerId +"");
            return ds;
        }



    }
}