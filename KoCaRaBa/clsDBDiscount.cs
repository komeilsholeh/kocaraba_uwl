﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace KoCaRaBa
{
    public class clsDBDiscount
    {
        public void addDiscount(clsDiscount discount)
        {
            clsDiscount newDiscount = discount;
            newDiscount.DiscountId = getNewDiscountId();
            string command = "INSERT INTO tblDiscount(DiscountId, ProductId, DiscountPercent, DiscountStartDate, DiscountFinishDate, DiscountValid) VALUES("+ newDiscount.DiscountId + "," + newDiscount.ProductId +"," + newDiscount.DiscountPercentage +",'"+ newDiscount.DiscountStartDate +"','"+ newDiscount.DiscountEndDate + "', " + newDiscount.DiscountValid + ")";

            clsDB db = new clsDB();
            db.DBExecute(command);

        }

        public DataSet getDiscountDB()
        {
            DataSet ds = new DataSet();
            clsDB db = new clsDB();
            ds = db.SelectCommand("SELECT * FROM tblDiscount");
            return ds;
        }

        public void deleteDiscount(clsDiscount discount)
        {
            clsDiscount newDiscount = discount;
            SqlCommand command = new SqlCommand("DELETE FROM tblDiscount WHERE DiscountId=@discountId");

            SqlParameter parameter = command.Parameters.Add("@discountId", SqlDbType.Int, newDiscount.DiscountId);

            clsDB db = new clsDB();
            db.updateCommand(command);

        }

        public Int32 getNewDiscountId()
        {
            clsDB db = new clsDB();
            DataSet ds = db.SelectCommand("SELECT MAX(DiscountId) AS Discountid FROM tblDiscount");
            Int32 disId = Convert.ToInt16(ds.Tables[0].Rows[0]["Discountid"]) + 1;
            return disId;

        }

    }
}