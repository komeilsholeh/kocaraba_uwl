﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KoCaRaBa
{
    class clsDBProduct
    {
        public void addProduct(clsProduct product)
        {
            clsProduct newproduct = product;
            newproduct.ProductId = getNewProductId();
            SqlCommand command = new SqlCommand("INSERT INTO tblProduct(productId,productName, productDescription,productImage,productUnit,productMin,categoryId,productPrice) VALUES(@productId,@productName, @productDescription,@productImage,@productUnit,@productMin,@categoryId,@productPrice)");

            // Add the parameters for the InsertCommand.
            //, ,,,,,
            command.Parameters.Add("@productId", SqlDbType.Int, newproduct.ProductId);
            command.Parameters.Add("@productName", SqlDbType.NVarChar, 50,newproduct.ProductName);
            command.Parameters.Add("@productDescription", SqlDbType.NVarChar, 700, newproduct.ProductDescription);
            command.Parameters.Add("@productImage", SqlDbType.NVarChar, 700, newproduct.ProductImage);
            command.Parameters.Add("@productUnit", SqlDbType.NVarChar, 10, newproduct.ProductUnit);
            command.Parameters.Add("@productMin", SqlDbType.Int, newproduct.ProductMin);
            command.Parameters.Add("@categoryId", SqlDbType.Int, newproduct.CategoryId);
            command.Parameters.Add("@productPrice", SqlDbType.Money, Convert.ToInt32(newproduct.ProductPrice));

            clsDB db = new clsDB();
            db.insertCommand(command);
        }

        public void updateProduct(clsProduct product)
        {
            clsProduct newproduct = product;
            SqlCommand command = new SqlCommand("UPDATE tblProduct SET productName=@productName, productDescription=@productDescription, productImage=@productImage, productUnit=@productUnit, productMin=@productMin, categoryId=,@categoryId, productPrice=@productPrice WHERE ProductId=@productId");

            // Add the parameters for the UpdateCommand.
            //, ,,,,,
            
            command.Parameters.Add("@productName", SqlDbType.NVarChar, 50, newproduct.ProductName);
            command.Parameters.Add("@productDescription", SqlDbType.NVarChar, 700, newproduct.ProductDescription);
            command.Parameters.Add("@productImage", SqlDbType.NVarChar, 700, newproduct.ProductImage);
            command.Parameters.Add("@productUnit", SqlDbType.NVarChar, 10, newproduct.ProductUnit);
            command.Parameters.Add("@productMin", SqlDbType.Int, newproduct.ProductMin);
            command.Parameters.Add("@categoryId", SqlDbType.Int, newproduct.CategoryId);
            command.Parameters.Add("@productPrice", SqlDbType.Money, Convert.ToInt32(newproduct.ProductPrice));

            SqlParameter parameter = command.Parameters.Add("@productId", SqlDbType.Int, newproduct.ProductId);

            clsDB db = new clsDB();
            db.updateCommand(command);
        }

        public void deleteProduct(clsProduct product)
        {
            clsProduct newproduct = product;
            SqlCommand command = new SqlCommand("DELETE FROM tblProduct WHERE ProductId=@productId");

            // Add the parameters for the deleteCommand.
            //, ,,,,,
            
            SqlParameter parameter = command.Parameters.Add("@productId", SqlDbType.Int, newproduct.ProductId);

            clsDB db = new clsDB();
            db.updateCommand(command);
        }

        public Int32 getCategoryId(string categoryName)
        {
            string catname = categoryName;
            clsDB db = new clsDB();
            DataSet ds = db.SelectCommand("SELECT CategoryId FROM tblCategory WHERE CategoryName='" + catname + "'");
            Int32 categoryId = Convert.ToInt16(ds.Tables[0].Rows[0].ItemArray[0]);
            return categoryId;
        }

        public Int32 getNewProductId()
        {
            clsDB db = new clsDB();
            DataSet ds = db.SelectCommand("SELECT MAX(ProductId) AS Productid FROM tblProduct");
            Int32 categoryId = Convert.ToInt16(ds.Tables[0].Rows[0]["Productid"]) + 1;
            return categoryId;
        }

        public Int32 getProductId(string productName)
        {
            string proname = productName;
            clsDB db = new clsDB();
            DataSet ds = db.SelectCommand("SELECT ProductId FROM tblProduct WHERE ProductName='" + proname + "'");
            Int32 productId = Convert.ToInt16(ds.Tables[0].Rows[0].ItemArray[0]);
            return productId;
        }
    }
    
}
