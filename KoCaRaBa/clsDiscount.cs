﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace KoCaRaBa
{
    public class clsDiscount
    {
        private Int32 discountId, productId, discountPercentage, discountValid;
        private string discountStartDate, discountEndDate;



        public void AddDiscount()
        {
            clsDBDiscount dbp = new clsDBDiscount();
            dbp.addDiscount(this);
        }

        public void deleteDiscount()
        {
            clsDBDiscount dbp = new clsDBDiscount();
            dbp.deleteDiscount(this);
        }

        public DataSet getData()
        {
            clsDBDiscount dbp = new clsDBDiscount();
            return dbp.getDiscountDB();
        }

        public int SelectCount()
        {
            int count = 0;
            DataSet ds;
            clsDB db = new clsDB();
            ds = db.SelectCommand("SELECT COUNT(*) FROM tblDiscount");
            count = int.Parse(ds.Tables[0].Rows[0][0].ToString());
            return count;
        }


        public int DiscountPercentage
        {
            get
            {
                return discountPercentage;
            }

            set
            {
                discountPercentage = value;
            }
        }

        public int DiscountValid
        {
            get
            {
                return discountValid;
            }

            set
            {
                discountValid = value;
            }
        }

        public int DiscountId
        {
            get
            {
                return discountId;
            }

            set
            {
                discountId = value;
            }
        }

        public int ProductId
        {
            get
            {
                return productId;
            }

            set
            {
                productId = value;
            }
        }

        public string DiscountStartDate
        {
            get
            {
                return discountStartDate;
            }

            set
            {
                discountStartDate = value;
            }
        }

        public string DiscountEndDate
        {
            get
            {
                return discountEndDate;
            }

            set
            {
                discountEndDate = value;
            }
        }

        public clsDiscount()
        {

        }

    }

}