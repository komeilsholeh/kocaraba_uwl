﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace KoCaRaBa
{
    public class clsPListGenerator
    {
        public string HtmlCodeProducts(DataSet productDataSet)
        {
            string currency = "£";
            string picfile = "";
            string commText = "";
            string price;
            string html = "<div class='row' style='padding-top:20px'>";
            for(int i=0;i< productDataSet.Tables[0].Rows.Count;i++)
            {
                picfile = "images/" + productDataSet.Tables[0].Rows[i]["ProductImage"].ToString() + "'";
                commText = productDataSet.Tables[0].Rows[i]["ProductId"].ToString();
                html += "<div class='col-sm-4 col-lg-4 col-md-4'><div class='thumbnail'>";
                html += "<img src='"+ picfile + "' style='width:270px; height:155px;'>";
                html += "&nbsp;<div class='caption'><h4 class='pull-rcd ight'>";
                price = productDataSet.Tables[0].Rows[i]["ProductPrice"].ToString();
                price = price.Substring(0, price.Length - 2);
                html += currency + price;
                html += "</h4><h4 ><a href='#'>";
                html += productDataSet.Tables[0].Rows[i]["ProductName"].ToString();
                html += "</a></h4><p>";
                html += productDataSet.Tables[0].Rows[i]["ProductDescription"].ToString();
                html += "</p></div><div class='container-fluid'>";
                html += "<button id='btnAdd" + commText + "' onclick='btnClick(id)'>Add</button>";
                html += "<input id='txtAdd" + commText + "' runat='server' name='txtAdd" + commText + "' type ='text' style ='width:35px; margin-top: 0' value='1' />";
                html += "</div></div></div>";
            }   
            html += "</div>";
            return html;
        }

        public string HtmlCodeCategoryMenu()
        {
            string htm = "";
            string catid;
            clsCategory cats = new clsCategory();
            DataSet ds = new DataSet();
            ds = cats.getCategoriesList();
     
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {  //id='btnAdd" + commText + "' onclick='btnClick(id)'
                catid = ds.Tables[0].Rows[i]["CategoryId"].ToString();
                htm += "<a href='#' class='list-group-item' id='CatLst" + catid + "' onclick='btnClick(id)' >";
                htm += ds.Tables[0].Rows[i]["CategoryName"].ToString() + "</a>";
            }               
                return htm;
        }
    }
}