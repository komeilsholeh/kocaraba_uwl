﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KoCaRaBa
{
    public class clsPayment
    {
        private int sellingId, productId, productQTY;
        private double soldPrice = 0.00;

        public void AddPaymentToDatabase()
        {
            clsDBPayment dbp = new clsDBPayment();
            dbp.addPayment(this);
        }




        public int ProductId
        {
            get
            {
                return productId;
            }

            set
            {
                productId = value;
            }
        }

        public int ProductQTY
        {
            get
            {
                return productQTY;
            }

            set
            {
                productQTY = value;
            }
        }

        public int SellingId
        {
            get
            {
                return sellingId;
            }

            set
            {
                sellingId = value;
            }
        }

        public double SoldPrice
        {
            get
            {
                return soldPrice;
            }

            set
            {
                soldPrice = value;
            }
        }

        public clsPayment()
        {

        }
    }
}