﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace KoCaRaBa
{
    public class clsProduct
    {
        private string productName, productDescription, productImage, productUnit, categoryName;
        private Int32 productId, categoryId, productMin;
        private double productPrice;



        public void AddProductToDatabase()
        {
            clsDBProduct dbp = new clsDBProduct();
            dbp.addProduct(this);
        }

        public void DeleteProductFromDatabase()
        {
            clsDBProduct dbp = new clsDBProduct();
            dbp.deleteProduct(this);
        }

        public void UpdateProductInDatabase()
        {
            clsDBProduct dbp = new clsDBProduct();
            dbp.updateProduct(this);
        }

        public string ProductName
        {
            get
            {
                return productName;
            }

            set
            {
                productName = value;
            }
        }

        public string ProductDescription
        {
            get
            {
                return productDescription;
            }

            set
            {
                productDescription = value;
            }
        }

        public string ProductImage
        {
            get
            {
                return productImage;
            }

            set
            {
                productImage = value;
            }
        }

        public string ProductUnit
        {
            get
            {
                return productUnit;
            }

            set
            {
                productUnit = value;
            }
        }

        public string CategoryName
        {
            get
            {
                return categoryName;
            }

            set
            {
                categoryName = value;
            }
        }

        public int ProductId
        {
            get
            {
               
                return productId;
            }

            set
            {
                productId = value;
            }
        }

        public int CategoryId
        {
            get
            {
                clsDBProduct dbproduct = new clsDBProduct();
                int catid = dbproduct.getCategoryId(categoryName);
                return catid;
            }

            set
            {
                categoryId = value;
            }
        }

        public int ProductMin
        {
            get
            {
                return productMin;
            }

            set
            {
                productMin = value;
            }
        }

        public double ProductPrice
        {
            get
            {
                return productPrice;
            }

            set
            {
                productPrice = value;
            }
        }

        public clsProduct()
        {


        }

    }


}