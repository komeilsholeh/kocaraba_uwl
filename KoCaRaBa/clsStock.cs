﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace KoCaRaBa
{
    public class clsStock
    {
        public DataSet SelectByCategoryId(int categoryId)
        {
            DataSet ds;
            clsDB db = new clsDB();
            ds = db.SelectCommand("SELECT * FROM qryProductStock WHERE CategoryId=" + categoryId + "AND ProductStock > 0");
            return ds;
        }

        public DataSet SearchByWord(string SearchWord)
        {
            DataSet ds;
            clsDB db = new clsDB();
            ds = db.SelectCommand("SELECT * FROM qryProductStock WHERE ProductName LIKE '%" + SearchWord + "%' AND ProductStock > 0");
            return ds;
        }

        public DataSet SelectAllInStock()
        {
            DataSet ds;
            clsDB db = new clsDB();
            ds = db.SelectCommand("SELECT * FROM qryProductStock WHERE ProductStock > 0");
            return ds;
        }

        public DataSet WordSearchStock(string word)
        {
            DataSet ds;
            clsDB db = new clsDB();
            ds = db.SelectCommand("SELECT * FROM qryProductStock WHERE ProductName LIKE '%" + word + "%'");
            return ds;
        }

    }
}